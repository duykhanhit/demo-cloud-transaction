const demoRouter = require('./demoRouter');
const tradeRouter = require('./tradeRouter');

module.exports = (app) => {
  app.use('/api/demo', demoRouter);
  app.use('/api/trade', tradeRouter)
}
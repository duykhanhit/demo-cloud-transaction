const express = require('express');
const tradeController = require('../controllers/tradeController');

const router = express.Router();

router
  .route('/')
  .post(tradeController.addTrade)

module.exports = router;
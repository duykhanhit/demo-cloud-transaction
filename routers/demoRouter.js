const express = require('express');
const demoController = require('../controllers/demoController');

const router = express.Router();

router
  .route('/')
  .post(demoController.addDemo)
  .get(demoController.getDemo)

module.exports = router;
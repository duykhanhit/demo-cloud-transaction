// Imports the Google Cloud client library
const { Datastore } = require('@google-cloud/datastore');

// Creates a client
const datastore = new Datastore({
  projectId: 'lucky-essence-291609',
  keyFilename: './key.json'
});

module.exports = datastore;
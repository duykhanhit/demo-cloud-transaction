const express = require('express');

const app = express();

const router = require('./routers/index');

app.use(express.json());
app.use(express.urlencoded({ extended: false }))

router(app);

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server test running on PORT ${PORT}`))
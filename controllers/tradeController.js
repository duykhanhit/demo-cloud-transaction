const datastore = require('../config/datastore')
const transaction = datastore.transaction();

module.exports = {
  addTrade: async (req, res) => {
    try {
      const { from, to, amount } = req.body;

      const fromKey = datastore.key(['transfer', from]);
      const toKey = datastore.key(['transfer', to]);

      await transaction.run();
      const results = await Promise.all([
        transaction.get(fromKey),
        transaction.get(toKey),
      ]);

      const accounts = results.map(result => result[0]);

      accounts[0].balance -= amount;
      accounts[1].balance += amount;

      await transaction.save([
        {
          key: fromKey,
          data: accounts[0],
        },
        {
          key: toKey,
          data: accounts[1],
        },
      ]);
      await transaction.commit();

      return res.json({
        success: true,
        data: `Transfer from ${from} to ${to} ${amount} vnđ.`
      })

    } catch (err) {
      await transaction.rollback();
      return res.json({
        success: false,
        data: err.message
      })
    }
  }
}
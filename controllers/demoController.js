const datastore = require('../config/datastore')

module.exports = {
  addDemo: async (req, res) => {
    try {

      const { kind, name, balance } = req.body;

      const taskKey = datastore.key([kind, name]);

      const task = {
        key: taskKey,
        data: {
          name: `Account ${name}`,
          balance: balance
        }
      }

      await datastore.insert(task);

      return res.status(201).json({
        success: true,
        data: task.data
      })

    } catch (err) {
      return res.status(400).json({
        success: false,
        data: err.message
      })
    }
  },

  getDemo: async (req, res) => {
    const transaction = datastore.transaction({ readOnly: true });
    try {
      const { kind, name } = req.query;

      const listKey = datastore.key([kind, name]);

      await transaction.run();
      const [taskList] = await transaction.get(listKey);
      await transaction.commit();

      res.json({
        success: true,
        data: taskList
      })
    } catch (err) {
      await transaction.rollback();
    }
  }
}